const axios = require('axios');

module.exports = {
	config: {
		name: "haremking",
		aliases: ["reg"],
		version: "1.0",
		author: "@tas33n",
		countDown: 5,
		role: 0,
		shortDescription: "Register for Harem Kings Throne",
		longDescription: "",
		category: "harem kings",
		guide: "{pn} Your Name"
	},

	onStart: async function ({ message, args, event }) {
    const name = args.join(" ");
		try {
			let res = await axios.get(`https://api.misfitsdev.xyz/harem/reg.php?uid=${event.senderID}&name=${name}`)
			let res2 = res.data

			const form = {
				body: res2.status.toString()
			};
			message.reply(form);
		} catch (e) {
			console.log(e)
			message.reply('🥺 server busy')
		}

	}
};